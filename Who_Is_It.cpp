#include<bits/stdc++.h>
using namespace std;

struct Student {
    int id;
    string name;
    string section;
    int totalMarks;
};

bool compareStudents(const Student& a, const Student& b) {
    if (a.totalMarks != b.totalMarks) {
        return a.totalMarks > b.totalMarks; 
    } else {
        return a.id < b.id;
    }
}

int main() {
    int T;
    cin >> T;

    while (T--) {
        vector<Student> students(3);

        for (int i = 0; i < 3; ++i) {
            cin >> students[i].id >> students[i].name >> students[i].section >> students[i].totalMarks;
        }

        sort(students.begin(), students.end(), compareStudents);

        cout << students[0].id << " " << students[0].name << " " << students[0].section << " " << students[0].totalMarks << endl;
    }

    return 0;
}
