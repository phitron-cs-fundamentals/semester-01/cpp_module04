#include <bits/stdc++.h>

using namespace std;

string c(const string &line)
{
    string input = line;
    input.erase(remove_if(input.begin(), input.end(), ::isspace), input.end());
    sort(input.begin(), input.end());
    return input;
}

int main()
{
    string line;
    while (getline(cin, line))
    {
        string ans = c(line);
        cout << ans << endl;
    }
    return 0;
}